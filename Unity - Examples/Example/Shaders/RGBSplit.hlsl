#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

float3 RGBSplit(float2 uv, float rgbSplit, uint rgbSplitSteps,float3 sceneColour)
{
    float3 rgbShift = 0.0;

    // Sample the opaque scene texture (scene color) using the Opaque Texture
    for (uint i = 0; i < rgbSplitSteps; i++)
    {
        float progress = (i + 1.0) / rgbSplitSteps;
        float offset = rgbSplit * progress;

        float2 offset_r = float2(-offset, -offset);
        float2 offset_g = float2(+0.0, +offset);
        float2 offset_b = float2(+offset, -offset);

        // Sample the Opaque Texture (scene color) with offsets for RGB channels
        rgbShift.r += SAMPLE_TEXTURE2D(sceneColour, sampler_CameraOpaqueTexture, uv - offset_r).r;
        rgbShift.g += SAMPLE_TEXTURE2D(sceneColour, sampler_CameraOpaqueTexture, uv - offset_g).g;
        rgbShift.b += SAMPLE_TEXTURE2D(sceneColour, sampler_CameraOpaqueTexture, uv - offset_b).b;
    }

    // Normalize by the number of steps
    rgbShift /= rgbSplitSteps;

    return rgbShift;
}
