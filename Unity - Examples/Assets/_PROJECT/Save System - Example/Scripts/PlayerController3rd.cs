using System;
using System.Collections;
using System.Collections.Generic;
using GizmoTools;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerController3rd : MonoBehaviour
{
    [Header ("THE PLAYER")]

    [SerializeField] float _speed;
    [SerializeField] float _turnspeed;

    [Header ("PLAYER SETTING")]
    [SerializeField] int _coinValue;
    [SerializeField] TextMeshProUGUI _coinText;
    [SerializeField] float _currentHP;
    [SerializeField] float _maxHP;
    [SerializeField] Image _hpImage;
    [SerializeField] Transform _rayStart;
    [SerializeField] float _rayDist;
    [SerializeField] LayerMask _layer;
    public bool _canWallRun;
    private int _coinTotal;

    public bool _saveActive { get; set; }

    private Vector3 _desiredMoveDirection;
    private Rigidbody _rb;
    private Animator _anim;
    private Transform _camera;
    private Vector2 _input;
    private ThirdPerson _inputsActions;
    private SaveLoadSystem _save;
    public float _damage;

    private void Awake ()
    {
        _save = FindObjectOfType<SaveLoadSystem> ();
        _inputsActions = new ThirdPerson ();
        _camera = Camera.main.transform;
        _anim = GetComponent<Animator> ();
        _rb = GetComponent<Rigidbody> ();
        _inputsActions.player.interact.performed += SavePlayerSettings;
        _save._loadEvent.AddListener (LoadPlayerSettings);
    }
    private void Start ()
    {
        UpdateCoinScore ();
        UpdateHealthUI ();
    }

    private void LoadPlayerSettings ()
    {
        Debug.Log ("LOAD CALLED");
        string dataToLoad = "";
        dataToLoad = _save.LoadData ();

        if (String.IsNullOrEmpty (dataToLoad) == false)
        {
            PlayerData _incomingSavedData = JsonUtility.FromJson<PlayerData> (dataToLoad);
            _coinTotal = _incomingSavedData._savedCoinTotal;
            transform.position = _incomingSavedData._savedPlayerPos;
            _currentHP = _incomingSavedData._savedHP;
        }
        else
        {
            _currentHP = _maxHP;
            _coinTotal = 0;
        }
        UpdateCoinScore ();
        UpdateHealthUI ();
    }

    void Update ()
    {
        _input = _inputsActions.player.move.ReadValue<Vector2> ();
        _anim.SetFloat ("speed", Mathf.Abs (_input.sqrMagnitude));
        _canWallRun = Physics.Raycast (_rayStart.position, _rayStart.forward, _rayDist, _layer);
    }

    void FixedUpdate ()
    {
        Movement ();
    }

    void Movement ()
    {
        _desiredMoveDirection = GizmoUtilities.CameraBasedInput (_camera, _input);
        Debug.Log (_desiredMoveDirection);

        if (_input != Vector2.zero && _canWallRun)
        {
            _rb.linearVelocity = new Vector3 (0, 1 * _speed * Time.fixedDeltaTime, 0);
        }
        else
        {
            _rb.linearVelocity = new Vector3 (_desiredMoveDirection.x * _speed * Time.fixedDeltaTime, _rb.linearVelocity.y, _desiredMoveDirection.z * _speed * Time.fixedDeltaTime);
        }

        if (_input != Vector2.zero)
        {
            transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (_desiredMoveDirection), _turnspeed * Time.fixedDeltaTime);
        }
    }

    public void SavePlayerSettings (InputAction.CallbackContext _incomingBool)
    {
        if (_incomingBool.performed && _saveActive)
        {
            PlayerData _newData = new PlayerData (_coinTotal, transform.position, _currentHP);
            string dataToSave = JsonUtility.ToJson (_newData);
            _save.SaveData (dataToSave);
        }
    }
    public void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag ("coin"))
        {
            _coinTotal += _coinValue;
            UpdateCoinScore ();
            Destroy (other.gameObject);
        }
    }

    private void OnTriggerStay (Collider other)
    {
        if (other.CompareTag ("hazard"))
        {
            _currentHP -= _damage;
            UpdateHealthUI ();
        }
    }

    private void UpdateCoinScore ()
    {
        _coinText.text = _coinTotal.ToString ();
    }

    private void UpdateHealthUI ()
    {
        _hpImage.fillAmount = _currentHP / _maxHP;
    }

    private void OnEnable ()
    {
        _inputsActions.Enable ();
    }

    private void OnDisable ()
    {
        _inputsActions.Disable ();
    }

    [Serializable]
    public class PlayerData
    {
        public int _savedCoinTotal;
        public Vector3 _savedPlayerPos;
        public float _savedHP;

        public PlayerData (int _coin, Vector3 _position, float _hp)
        {
            _savedCoinTotal = _coin;
            _savedPlayerPos = _position;
            _savedHP = _hp;
        }
    }
}