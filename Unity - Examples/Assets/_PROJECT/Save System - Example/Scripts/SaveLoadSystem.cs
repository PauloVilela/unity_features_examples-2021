using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

public class SaveLoadSystem : MonoBehaviour
{
    [SerializeField] string _saveFileName = "savedFile_";
    [Range (0, 2)]
    public int _saveSlotIndex = 0;
    [HideInInspector]
    public UnityEvent _saveEvent = new UnityEvent ();
    [HideInInspector]
    public UnityEvent _loadEvent = new UnityEvent ();

    private void Start ()
    {
        LoadGame ();
    }

    private void SaveGame ()
    {
        // Send message to all subscribers to save data
        _saveEvent.Invoke ();
    }

    private void LoadGame ()
    {
        // Send message to all subscribers to load data
        _loadEvent.Invoke ();
    }

    public void SaveData (string _dataToSave)
    {
        if (WriteTofile (_saveFileName + _saveSlotIndex, _dataToSave))
        {
            Debug.Log ("SAVE SUCCESSFUL");
        }
    }

    private bool WriteTofile (string name, string savedContent)
    {
        var savePath = Path.Combine (Application.persistentDataPath, name);

        try
        {
            File.WriteAllText (savePath, savedContent);
            return true;
        }
        catch (Exception)
        {
            Debug.Log ("UNABLE TO SAVE");
        }
        return false;
    }

    public string LoadData ()
    {
        string data = "";
        if (ReadFromFile (_saveFileName + _saveSlotIndex, out data))
        {
            Debug.Log ("SUCCESSFUL LOAD");
        }
        return data;
    }

    private bool ReadFromFile (string name, out string loadContent)
    {
        var savePath = Path.Combine (Application.persistentDataPath, name);
        try
        {
            loadContent = File.ReadAllText (savePath);
            return true;
        }
        catch (Exception)
        {
            Debug.Log ("NO FILE TO LOAD");
        }

        loadContent = null;
        return false;
    }
}