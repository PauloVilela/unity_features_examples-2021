using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GizmoTools
{
    public class GizmoUtilities
    {
        private static Vector2 _inputs;
        private static Transform _camera;
        private static Vector3 _cameraForward;
        private static Vector3 _cameraRight;
        private static Vector3 _cameraBasedDirection;

        public static Vector3 CameraBasedInput (Transform _incomingCamera, Vector2 _incomingInput)
        {
            _camera = _incomingCamera;
            _inputs = _incomingInput;
            _cameraForward = _camera.forward;
            _cameraRight = _camera.right;
            _cameraForward.y = 0;
            _cameraRight.y = 0;
            _cameraForward = _cameraForward.normalized;
            _cameraRight = _cameraRight.normalized;
            _cameraBasedDirection = _cameraForward * _inputs.y + _cameraRight * _inputs.x;
            return _cameraBasedDirection;
        }
    }
}