using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SaveFire : MonoBehaviour
{
    [SerializeField] GameObject _fire;
    [SerializeField] UnityEvent _event;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController3rd>()._saveActive = true;
            _event.Invoke();
            Debug.Log("IN RANGE");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController3rd>()._saveActive = false;
            _fire.SetActive(false);
        }
    }
}