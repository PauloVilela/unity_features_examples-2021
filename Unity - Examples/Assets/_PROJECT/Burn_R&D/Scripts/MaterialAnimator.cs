using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialAnimator : MonoBehaviour
{
    [SerializeField] float lerpSpeed = 0.1f;
    [SerializeField] float end = 1;
    [SerializeField] string nameOfMaterialParameter;
    [SerializeField] Transform hitPoint;

    private Material materialRef;
    private float currentLerp;

    private void Awake()
    {
        materialRef = GetComponent<MeshRenderer>().material;
        currentLerp = materialRef.GetFloat(nameOfMaterialParameter);
        materialRef.SetVector("impactPoint", hitPoint.position);
    }

    private void Update()
    {
        if (currentLerp < end)
        {
            currentLerp += lerpSpeed * Time.deltaTime;
            materialRef.SetFloat(nameOfMaterialParameter, currentLerp);
        }
    }
}
