using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Position : MonoBehaviour {
    [SerializeField] Material screenEffect;
    void Update() {
        screenEffect.SetVector ("_PlayerPosition", transform.position);
    }
}
