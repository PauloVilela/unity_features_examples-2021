using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FPScontroller : MonoBehaviour
{
    [SerializeField] float speedMultiplier;
    [SerializeField] float lookSpeed;
    [SerializeField] Transform cameraTransform;
    private FPSactions actions;
    private Vector2 inputMove;
    private Vector2 inputLook;
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        actions = new FPSactions();
        actions.Enable();
        actions.fps.move.performed += OnMove;
        actions.fps.look.performed += OnLook;
        actions.fps.look.canceled += OnLook;
        CrocodileBrackets.Utilities.CursorSettings(false, CursorLockMode.Locked);
    }

    private void OnLook(InputAction.CallbackContext obj)
    {
        inputLook = obj.ReadValue<Vector2>();
    }

    private void OnMove(InputAction.CallbackContext obj)
    {
        inputMove = obj.ReadValue<Vector2>();
    }

    private void FixedUpdate()
    {
        Vector3 directionalInput = CrocodileBrackets.Utilities.CameraBasedInput(cameraTransform, inputMove);
        rb.linearVelocity = new Vector3(directionalInput.x * speedMultiplier * Time.fixedDeltaTime, rb.linearVelocity.y, directionalInput.z * speedMultiplier * Time.fixedDeltaTime);
    }

    private void LateUpdate()
    {
        transform.Rotate(new Vector3(0, inputLook.x * lookSpeed * Time.deltaTime, 0), Space.Self);
        cameraTransform.Rotate(new Vector3(-inputLook.y * lookSpeed * Time.deltaTime, 0, 0), Space.Self);
    }
}
