using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BuoyancyObject : MonoBehaviour
{
    public List<Transform> floaters = new List<Transform>();

    public float underWaterDrag = 3;
    public float underWaterAngularDrag = 1;
    public float airDrag = 0;
    public float airAngularDrag = 0.5f;
    public float floatingPower = 15;
    public float waterHeight;

    private Rigidbody m_rigidbody;
    private bool underWater;
    private int floatersUnderWater;

    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        floatersUnderWater = 0;
        for (var i = 0; i < floaters.Count; i++)
        {
            float difference = floaters[i].position.y - waterHeight;

            if (difference < 0)
            {
                m_rigidbody.AddForceAtPosition(Vector3.up * floatingPower * Mathf.Abs(difference), floaters[i].position, ForceMode.Force);
                floatersUnderWater++;
                if (!underWater)
                {
                    underWater = true;
                    SwitchState(underWater);
                }
            }
        }
        if (underWater && floatersUnderWater == 0)
        {
            underWater = false;
            SwitchState(underWater);
        }
    }

    private void SwitchState(bool isUnderWater)
    {
        if (isUnderWater)
        {
            m_rigidbody.linearDamping = underWaterDrag;
            m_rigidbody.angularDamping = underWaterAngularDrag;
        }
        else
        {
            m_rigidbody.linearDamping = airDrag;
            m_rigidbody.angularDamping = airAngularDrag;
        }
    }
}
