void RGBSplit(float4 sceneColor, float2 uv, float rgbSplit, uint rgbSplitSteps, out float3 rgbShift)
{
    rgbShift = float3(0.0, 0.0, 0.0);

    for (uint i = 0; i < rgbSplitSteps; i++)
    {
        float progress = (i + 1.0) / rgbSplitSteps;
        
        float offset = rgbSplit * progress;
        
        float2 offset_r = float2(-offset, -offset);
        float2 offset_g = float2(+0.0, +offset);
        float2 offset_b = float2(+offset, -offset);
        
        rgbShift.r += sceneColor * (uv - offset_r).r;
        rgbShift.g += sceneColor * (uv - offset_g).g;
        rgbShift.b += sceneColor * (uv - offset_b).b;
    }
    
    rgbShift /= rgbSplitSteps;
}

void SplitChannelBlur(float2 uv, float rgbSplit, uint rgbSplitSteps, float offsetMin,float offsetmax,out float2 rgbShift)
{
    rgbShift = float3(0.0, 0.0, 0.0);

    for (uint i = 0; i < rgbSplitSteps; i++)
    {
        float progress = (i + 1.0) / rgbSplitSteps;
        
        float offset = rgbSplit * progress;
        
        float2 offsetChannel = float2(offset * offsetMin, offset * offsetmax);

        rgbShift += uv - offsetChannel;     
    }
    rgbShift /= rgbSplitSteps;
}
float3 TwistXZ(float3 position, float twist)
{
float angle = position.y * twist;
float cosAngle = cos(angle);
float sinAngle = sin(angle);

float3 twistedPosition;

twistedPosition.x = (position.x * cosAngle) - (position.z * sinAngle);
twistedPosition.y = position.z;
twistedPosition.z = (position.x * sinAngle) - (position.z * cosAngle);

return twistedPosition;
}