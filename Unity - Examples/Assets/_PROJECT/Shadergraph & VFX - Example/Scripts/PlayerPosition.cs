using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPosition : MonoBehaviour
{
    [SerializeField] Material _mat;

    void Update ()
    {
        _mat.SetVector ("_PlayerPos", transform.position);
    }
}