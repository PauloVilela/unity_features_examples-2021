using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PainterPosition : MonoBehaviour
{
    public Camera paintCam;
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            transform.position = CrocodileBrackets.Utilities.ScreenToWorldPosition3D(paintCam, Input.mousePosition);
        }
    }
}
