using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SelectPlayableCharacterPrefab : MonoBehaviour
{
    [SerializeField] GameObject nextPlayerPrefab;
    private PlayerInputManager manager;

    private void Awake()
    {
        manager = GetComponent<PlayerInputManager>();
    }

    public void UpdateCurrentPlayerPrefab()
    {
        manager.playerPrefab = nextPlayerPrefab;
    }
}
