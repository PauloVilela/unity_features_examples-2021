using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCameraFollowCoop : MonoBehaviour
{
    [SerializeField] float _lerpSpeed = 5f;
    public Transform _playerRef { get; set; }
    private Vector3 _offset;

    public void UpdateCAmeraOffset ()
    {
        _offset = transform.position - _playerRef.position;
    }

    void LateUpdate ()
    {
        transform.position = Vector3.Lerp (transform.position, _playerRef.position + _offset, _lerpSpeed * Time.deltaTime);
    }
}