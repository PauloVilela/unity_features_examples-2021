using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputControllerCoop : MonoBehaviour
{
    [SerializeField] float _carSpeed;
    [SerializeField] float _steerAngle;
    [SerializeField] Transform _centerOfMass;
    [SerializeField] GameObject _camera;
    private float _steeringInput { get; set; }
    private float _speedInput { get; set; }
    private CarInputActions _inputs;
    private Rigidbody _rb;
    [SerializeField] List<Wheel> _wheels = new List<Wheel> ();

    private void Awake ()
    {
        _rb = GetComponent<Rigidbody> ();
        _rb.centerOfMass = _centerOfMass.localPosition;
        _inputs = new CarInputActions ();
        GameObject _cam = Instantiate (_camera, _camera.transform.position, _camera.transform.rotation);
        _cam.GetComponent<CarCameraFollowCoop> ()._playerRef = transform;
        _cam.GetComponent<CarCameraFollowCoop> ().UpdateCAmeraOffset ();
    }

    private void Update ()
    {
        foreach (var wheel in _wheels)
        {
            wheel._steerAngle = _steeringInput * _steerAngle;
            wheel._torque = _speedInput * _carSpeed;
        }
    }

    public void UpdateSteering (InputAction.CallbackContext _incoming)
    {
        _steeringInput = _incoming.ReadValue<float> ();
    }

    public void UpdateTorque (InputAction.CallbackContext _incoming)
    {
        _speedInput = _incoming.ReadValue<float> ();
    }

    private void OnEnable ()
    {
        _inputs.Enable ();
    }

    private void OnDisable ()
    {
        _inputs.Disable ();
    }
}