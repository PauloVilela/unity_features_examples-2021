using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputController : MonoBehaviour
{
    [SerializeField] float _carSpeed;
    [SerializeField] float _steerAngle;
    [SerializeField] Transform _centerOfMass;
    private float _steeringInput { get; set; }
    private float _speedInput { get; set; }
    private CarInputActions _inputs;
    private Rigidbody _rb;
    [SerializeField] List<Wheel> _wheels = new List<Wheel> ();

    private void Awake ()
    {
        _rb = GetComponent<Rigidbody> ();
        _rb.centerOfMass = _centerOfMass.localPosition;
        _inputs = new CarInputActions ();
    }

    private void Update ()
    {
        _steeringInput = _inputs.car.steer.ReadValue<float> ();
        _speedInput = _inputs.car.speed.ReadValue<float> ();

        foreach (var wheel in _wheels)
        {
            wheel._steerAngle = _steeringInput * _steerAngle;
            wheel._torque = _speedInput * _carSpeed;
        }
    }

    private void OnEnable ()
    {
        _inputs.Enable ();
    }

    private void OnDisable ()
    {
        _inputs.Disable ();
    }
}