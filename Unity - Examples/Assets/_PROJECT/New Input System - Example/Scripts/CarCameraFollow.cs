using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCameraFollow : MonoBehaviour
{
    [SerializeField] float _lerpSpeed = 5f;
    private Transform _playerRef;
    private Vector3 _offset;

    private void Awake ()
    {
        _playerRef = FindObjectOfType<InputController> ().transform;
        _offset = transform.position - _playerRef.position;
    }

    void LateUpdate ()
    {
        transform.position = Vector3.Lerp (transform.position, _playerRef.position + _offset, _lerpSpeed * Time.deltaTime);
    }
}