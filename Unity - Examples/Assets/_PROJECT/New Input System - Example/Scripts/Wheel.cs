using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour
{
    public bool _steer;
    public bool _invertSteer;
    public bool _power;
    public float _steerAngle { get; set; }
    public float _torque { get; set; }

    private WheelCollider _wheelCollider;
    private Transform _wheelTransform;

    private void Awake ()
    {
        _wheelCollider = GetComponent<WheelCollider> ();
        _wheelTransform = GetComponentInChildren<MeshRenderer> ().GetComponent<Transform> ();
    }

    private void Update ()
    {
        _wheelCollider.GetWorldPose (out Vector3 pos, out Quaternion rot);
        _wheelTransform.position = pos;
        _wheelTransform.rotation = rot;
    }

    private void FixedUpdate ()
    {
        if (_steer)
        {
            _wheelCollider.steerAngle = _steerAngle * (_invertSteer? - 1 : 1);
        }

        if (_power)
        {
            _wheelCollider.motorTorque = _torque;
        }
    }
}