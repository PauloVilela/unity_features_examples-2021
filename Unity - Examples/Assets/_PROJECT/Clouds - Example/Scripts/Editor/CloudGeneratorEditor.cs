using System.Collections;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (CloudGenerator))]
public class CloudGeneratorEditor : Editor
{
    public override void OnInspectorGUI ()
    {
        DrawDefaultInspector ();

        CloudGenerator _script = (CloudGenerator) target;

        if (GUILayout.Button ("Generate Clouds"))
        {
            _script.GenerateClouds ();
        }

        if (GUILayout.Button ("Clear Clouds"))
        {
            _script.ClearClouds ();
        }
    }
}