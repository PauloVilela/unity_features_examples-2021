using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudScript : MonoBehaviour
{
    [SerializeField] float _minSize;
    [SerializeField] float _maxSize;
    [SerializeField] float _yscale;
    [SerializeField] float _cloudSpeed;

    public void CloudScale ()
    {
        float _randomSize = Random.Range (_minSize, _maxSize);
        Vector3 _randomSizeV3 = new Vector3 (_randomSize, _randomSize / _yscale, _randomSize);
        transform.localScale = _randomSizeV3;
    }

    void Update ()
    {
        transform.Translate (Vector3.right * _cloudSpeed * Time.deltaTime);
    }
}