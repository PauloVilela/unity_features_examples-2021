using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FlyingController : MonoBehaviour
{
    [SerializeField] float _falconSpeedMultiplier;
    [SerializeField] float _rotateMultiplier;
    [SerializeField] float _clampedValues;
    private Rigidbody _rb;
    private FalconActions _inputs;
    private Vector2 _movementInput;
    private float _rotationX;
    private float _rotationY;
    private Animator _anim;

    private void Awake ()
    {
        _rb = GetComponent<Rigidbody> ();
        _inputs = new FalconActions ();
        _anim = GetComponentInChildren<Animator> ();
    }

    private void Update ()
    {
        _movementInput = _inputs.player.rotate.ReadValue<Vector2> ();
        _anim.SetFloat ("rotateDirection", _movementInput.x);

        if (_movementInput.x != 0)
        {
            _anim.SetBool ("Rotating", true);
        }
        if (_movementInput.x == 0)
        {
            _anim.SetBool ("Rotating", false);
        }
    }

    private void FixedUpdate ()
    {
        _rb.linearVelocity = transform.forward * _falconSpeedMultiplier * Time.fixedDeltaTime;

        _rotationX += _movementInput.y * _rotateMultiplier * Time.fixedDeltaTime;
        _rotationX = Mathf.Clamp (_rotationX, -_clampedValues, _clampedValues);
        _rotationY += _movementInput.x * _rotateMultiplier * Time.fixedDeltaTime;
        transform.eulerAngles = new Vector3 (_rotationX, _rotationY, 0);
    }

    private void OnEnable ()
    {
        _inputs.Enable ();
    }

    private void OnDisable ()
    {
        _inputs.Disable ();
    }
}