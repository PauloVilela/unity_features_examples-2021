using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudGenerator : MonoBehaviour
{
    [SerializeField] float _spawnHeight;
    [SerializeField] float _heightVariation;
    [SerializeField] float _spawnArea;
    [SerializeField] GameObject _cloudPrefab;
    [SerializeField] int _cloudAmount;
    private List<GameObject> _cloudList = new List<GameObject> ();

    public void GenerateClouds ()
    {
        for (int i = 0; i < _cloudAmount; i++)
        {
            Vector3 _spawnPos = new Vector3 (Random.Range (transform.position.x - _spawnArea, transform.position.x + _spawnArea), Random.Range (_spawnHeight - _heightVariation, _spawnHeight + _heightVariation), Random.Range (transform.position.z - _spawnArea, transform.position.z + _spawnArea));
            GameObject _currentCloud = Instantiate (_cloudPrefab, _spawnPos, Quaternion.identity, transform);
            _currentCloud.GetComponent<CloudScript> ().CloudScale ();
            _cloudList.Add (_currentCloud);
        }
    }

    public void ClearClouds ()
    {
        Debug.Log ("CLEAR OUT THE LIST");
        foreach (GameObject child in _cloudList)
        {
            DestroyImmediate (child.gameObject);
        }
        _cloudList.Clear ();
    }
}