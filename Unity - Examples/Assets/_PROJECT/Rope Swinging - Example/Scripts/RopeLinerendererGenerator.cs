﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeLinerendererGenerator : MonoBehaviour
{
    public RopeSegment[] _ropeSegments;
    private LineRenderer _line;

    private void Awake ()
    {
        _ropeSegments = transform.GetComponentsInChildren<RopeSegment> ();
        _line = GetComponent<LineRenderer> ();
        _line.positionCount = _ropeSegments.Length;
        for (var i = 0; i < _ropeSegments.Length; i++)
        {
            _line.SetPosition (i, _ropeSegments[i].transform.position);
        }
    }

    private void LateUpdate ()
    {
        for (var i = 0; i < _ropeSegments.Length; i++)
        {
            _line.SetPosition (i, _ropeSegments[i].transform.position);
        }
    }
}