﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeSegment : MonoBehaviour
{
    public GameObject _connectedAbove, _connectBelow;
    public bool _isPlayerAttached = false;

    private void Start ()
    {
        _connectedAbove = GetComponent<HingeJoint2D> ().connectedBody.gameObject;
        RopeSegment _aboveSegment = _connectedAbove.GetComponent<RopeSegment> ();

        if (_aboveSegment != null)
        {
            _aboveSegment._connectBelow = gameObject;
            float _spriteBottom = _connectedAbove.GetComponent<SpriteRenderer> ().bounds.size.y;
            GetComponent<HingeJoint2D> ().connectedAnchor = new Vector2 (0, -_spriteBottom);
        }
        else
        {
            GetComponent<HingeJoint2D> ().connectedAnchor = new Vector2 (0, 0);
        }
    }
}