﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlatformerPlayerController : MonoBehaviour
{
    [SerializeField] float _speedMultiplier = 50;
    [SerializeField] float _jumpMultiplier = 5;
    [SerializeField] float _swingMultiplier = 10;
    [SerializeField] float _rayRadius = 0.2f;
    [SerializeField] Transform _groundcheckPos;
    [SerializeField] LayerMask _layer;

    public Transform _attachedTo;

    private bool _onRope;
    private Rigidbody2D _rb2d;
    public HingeJoint2D _hj;
    private PlayerPlatformer _inputs2d;
    private float _playerMoveInput;
    private bool _grounded = true;
    private Animator _anim;

    private void Awake ()
    {
        _inputs2d = new PlayerPlatformer ();
        _rb2d = GetComponent<Rigidbody2D> ();
        _hj = GetComponent<HingeJoint2D> ();
        _anim = GetComponent<Animator> ();
        _inputs2d.player.jump.performed += OnJump;
    }

    private void OnJump (InputAction.CallbackContext _jump)
    {
        if (_grounded)
        {
            _rb2d.AddForce (Vector2.up * _jumpMultiplier, ForceMode2D.Impulse);
        }
        if (_onRope)
        {
            DetachFromRope ();
        }
    }

    private void AttachToRope (Rigidbody2D _rope)
    {
        _rope.gameObject.GetComponent<RopeSegment> ()._isPlayerAttached = true;
        _hj.connectedBody = _rope;
        _hj.enabled = true;
        _onRope = true;
        _attachedTo = _rope.gameObject.transform.parent;
    }

    private void DetachFromRope ()
    {
        _hj.connectedBody.gameObject.GetComponent<RopeSegment> ()._isPlayerAttached = false;
        _hj.enabled = false;
        _hj.connectedBody = null;
        StartCoroutine (DetachDelay ());
    }

    private IEnumerator DetachDelay ()
    {
        yield return new WaitForSeconds (0.5f);
        _onRope = false;
        _attachedTo = null;
    }

    void Update ()
    {
        _playerMoveInput = _inputs2d.player.move.ReadValue<float> ();
        _anim.SetFloat ("speed", Mathf.Abs (_playerMoveInput));

        if (_playerMoveInput > 0)
        {
            transform.localScale = new Vector3 (1, transform.localScale.y, transform.localScale.z);
        }
        if (_playerMoveInput < 0)
        {
            transform.localScale = new Vector3 (-1, transform.localScale.y, transform.localScale.z);
        }
        _grounded = Physics2D.OverlapCircle (_groundcheckPos.position, _rayRadius, _layer);
    }

    private void FixedUpdate ()
    {
        if (_onRope)
        {
            _rb2d.AddRelativeForce (new Vector2 (_playerMoveInput * _swingMultiplier * Time.fixedDeltaTime, 0));
        }
        else
        {
            _rb2d.linearVelocity = new Vector2 (_playerMoveInput * _speedMultiplier * Time.fixedDeltaTime, _rb2d.linearVelocity.y);
        }
    }

    private void OnTriggerEnter2D (Collider2D other)
    {
        if (!_onRope)
        {
            if (other.CompareTag ("rope") && _attachedTo != other.transform.parent)
            {
                AttachToRope (other.gameObject.GetComponent<Rigidbody2D> ());
            }
        }
    }

    private void OnEnable ()
    {
        _inputs2d.Enable ();
    }

    private void OnDisable ()
    {
        _inputs2d.Disable ();
    }
}