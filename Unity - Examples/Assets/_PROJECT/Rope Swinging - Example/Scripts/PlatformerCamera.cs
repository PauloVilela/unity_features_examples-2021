using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerCamera : MonoBehaviour
{
    [SerializeField] float _lerpSpeed = 5f;
    public Transform _playerRef;
    private Vector3 _offset;

    public void Start ()
    {
        _offset = transform.position - _playerRef.position;
    }

    public void LateUpdate ()
    {
        transform.position = Vector3.Lerp (transform.position, _playerRef.position + _offset, _lerpSpeed * Time.deltaTime);
    }
}