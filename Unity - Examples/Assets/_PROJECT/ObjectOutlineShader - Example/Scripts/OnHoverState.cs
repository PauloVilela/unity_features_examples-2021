using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OnHoverState : MonoBehaviour
{
    [SerializeField] Material material;
    [SerializeField] float tweenTime = 1;
    [SerializeField] float defaultValue = 1f;
    [SerializeField] float highlightValue = 1.05f;

    private void OnMouseEnter()
    {
        material.DOFloat(highlightValue, "_OutlineThickness", tweenTime);
    }
    private void OnMouseExit()
    {
        material.DOFloat(defaultValue, "_OutlineThickness", tweenTime);
    }
}
