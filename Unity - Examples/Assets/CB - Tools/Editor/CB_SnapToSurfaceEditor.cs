using UnityEngine;
using UnityEditor;

public class CB_SnapToSurfaceEditor : EditorWindow
{
    private bool snapEnabled = false; // Toggle for snapping
    private LayerMask snapLayerMask = -1; // Layer mask for determining which surfaces can be snapped to

    [MenuItem("Tools/CB/Snap to Surface")]
    public static void ShowWindow()
    {
        GetWindow<CB_SnapToSurfaceEditor>("Snap to Surface");
    }

    void OnEnable()
    {
        Selection.selectionChanged += OnSelectionChanged;
    }

    void OnDisable()
    {
        Selection.selectionChanged -= OnSelectionChanged;
    }

    void OnGUI()
    {
        GUILayout.Label("Snap to Surface", EditorStyles.boldLabel);

        snapLayerMask = EditorGUILayout.LayerField("Snap Layer Mask", snapLayerMask);

        if (GUILayout.Button(snapEnabled ? "Disable Snap" : "Enable Snap"))
        {
            snapEnabled = !snapEnabled;
        }
    }

    private void OnSelectionChanged()
    {
        if (!snapEnabled)
            return;

        GameObject selectedObject = Selection.activeGameObject;
        if (selectedObject != null)
        {
            SnapObjectToSurface(selectedObject);
        }
    }

    private void SnapObjectToSurface(GameObject obj)
    {
        // Perform a raycast from above the object, downwards, to find the surface
        Ray ray = new Ray(obj.transform.position + Vector3.up * 10, Vector3.down); // Cast from above
        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, snapLayerMask))
        {
            // Move the object to the hit point
            Undo.RecordObject(obj.transform, "Snap to Surface");
            obj.transform.position = hit.point;

            // Optional: Orient the object to align with the surface normal
            obj.transform.up = hit.normal;

            Debug.Log("Object snapped to surface at position: " + hit.point);
        }
        else
        {
            Debug.LogWarning("No surface found below the object to snap to.");
        }
    }
}
