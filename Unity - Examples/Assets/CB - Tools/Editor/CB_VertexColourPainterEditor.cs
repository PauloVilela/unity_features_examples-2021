using UnityEngine;
using UnityEditor;

public class CB_VertexColourPainterEditor : EditorWindow
{
    private Color paintColor = Color.red;
    private float brushSize = 1f;
    private float brushStrength = 1f;

    private MeshFilter meshFilter;
    private bool paintingEnabled = false;
    private Vector3 hitPoint;
    Color activeColour = new Color(0.5686275f, 0.2980392f, 0.9803922f, 1f);

    [MenuItem("Tools/CB/Vertex Colour Painter")]
    public static void ShowWindow()
    {
        GetWindow<CB_VertexColourPainterEditor>("Vertex Colour Painter");
    }

    void OnGUI()
    {
        GUILayout.Label("Vertex Colour Painter", EditorStyles.boldLabel);
        paintColor = EditorGUILayout.ColorField("Paint Color", paintColor);
        brushSize = EditorGUILayout.FloatField("Brush Size", brushSize);
        brushStrength = EditorGUILayout.FloatField("Brush Strength", brushStrength);

        GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);

        if (paintingEnabled)
        {
            buttonStyle.normal.background = MakeTex(2, 2, activeColour);
            buttonStyle.normal.textColor = Color.white; // White text when active
        }

        if (GUILayout.Button(paintingEnabled ? "Disable Painting" : "Enable Painting", buttonStyle))
        {
            TogglePainting();
        }

        if (GUILayout.Button("Fill Entire Mesh"))
        {
            if (meshFilter != null)
            {
                Undo.RegisterCompleteObjectUndo(meshFilter.sharedMesh, "Fill Mesh Color");
                FillEntireMesh(meshFilter.sharedMesh, paintColor);
                EditorUtility.SetDirty(meshFilter.sharedMesh);
                SaveMesh(meshFilter.sharedMesh); // Save changes to the mesh
                SceneView.RepaintAll();
            }
        }
    }

    void TogglePainting()
    {
        paintingEnabled = !paintingEnabled;
        if (paintingEnabled)
            SceneView.duringSceneGui += OnSceneGUI;
        else
            SceneView.duringSceneGui -= OnSceneGUI;
    }

    void OnSceneGUI(SceneView sceneView)
    {
        if (!paintingEnabled)
            return;

        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        Event e = Event.current;

        Ray ray = HandleUtility.GUIPointToWorldRay(e.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            hitPoint = hit.point;
            Handles.color = new Color(paintColor.r, paintColor.g, paintColor.b, 0.5f); // Set handle color
            Handles.DrawWireDisc(hitPoint, hit.normal, brushSize); // Draw the brush size circle

            if (e.type == EventType.MouseDown && e.button == 0)
                StartPainting(hit);
            else if (e.type == EventType.MouseDrag && e.button == 0)
                StartPainting(hit);
        }

        sceneView.Repaint();
    }

    void StartPainting(RaycastHit hit)
    {
        meshFilter = hit.collider.GetComponent<MeshFilter>();

        if (meshFilter != null)
            PaintOnMesh(meshFilter.sharedMesh, hit);
    }

    void PaintOnMesh(Mesh mesh, RaycastHit hit)
    {
        Vector3[] vertices = mesh.vertices;
        Color[] colors = mesh.colors;

        if (colors.Length == 0)
        {
            colors = new Color[vertices.Length];
            for (int i = 0; i < colors.Length; i++)
                colors[i] = Color.white;
        }

        Transform hitTransform = hit.collider.transform;
        Vector3 hitPointLocal = hitTransform.InverseTransformPoint(hit.point);

        // Register undo before making changes
        Undo.RegisterCompleteObjectUndo(mesh, "Vertex Paint");

        for (int i = 0; i < vertices.Length; i++)
        {
            float distance = Vector3.Distance(vertices[i], hitPointLocal);
            if (distance < brushSize)
            {
                colors[i] = Color.Lerp(colors[i], paintColor, brushStrength * (1 - distance / brushSize));
            }
        }

        mesh.colors = colors;
        EditorUtility.SetDirty(mesh);
        SaveMesh(mesh); // Save changes to the mesh
        SceneView.RepaintAll();
    }

    // Method to fill the entire mesh with the selected color
    void FillEntireMesh(Mesh mesh, Color color)
    {
        Vector3[] vertices = mesh.vertices;
        Color[] colors = new Color[vertices.Length];

        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = color;
        }

        mesh.colors = colors;
        EditorUtility.SetDirty(mesh);
        SaveMesh(mesh); // Save changes to the mesh
    }

    // Helper function to create a texture with a specific color
    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }

    private void OnDestroy()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
        paintingEnabled = false;
    }

    private void SaveMesh(Mesh mesh)
    {
        string path = AssetDatabase.GetAssetPath(mesh);
        if (string.IsNullOrEmpty(path))
        {
            path = EditorUtility.SaveFilePanelInProject("Save Mesh", mesh.name, "asset", "Please enter a file name to save the mesh.");
            if (string.IsNullOrEmpty(path)) return;

            AssetDatabase.CreateAsset(mesh, path);
            Debug.Log("New Mesh created: " + path);
        }
        else
        {
            AssetDatabase.SaveAssets();
            Debug.Log("Mesh already exists: " + path);
        }
    }
}
