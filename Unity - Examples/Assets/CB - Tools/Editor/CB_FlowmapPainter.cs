using UnityEngine;
using UnityEditor;

public class CB_FlowmapPainter : EditorWindow
{
    private GameObject targetObject;
    public Texture2D flowMap;
    private Vector2 lastMousePosition;
    private bool isPainting = false;
    private float brushSize = 10f;
    private float brushStrength = 0.5f;
    private bool showVisualization = true;
    private Material visualizationMaterial;
    private float visualizationStrength = 0.5f;
    private float visualizationAlpha = 0.7f;

    [MenuItem("Tools/CB/URP Flow Map Painter")]
    public static void ShowWindow()
    {
        GetWindow<CB_FlowmapPainter>("URP Flow Map Painter");
    }

    private void OnEnable()
    {
        SceneView.duringSceneGui += OnSceneGUI;
        CreateVisualizationMaterial();
    }

    private void OnDisable()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
        DestroyImmediate(visualizationMaterial);
    }

    private void CreateVisualizationMaterial()
    {
        Shader shader = Shader.Find("Custom/URPFlowMapVisualization");
        if (shader == null)
        {
            Debug.LogError("Custom/URPFlowMapVisualization shader not found. Make sure it's in your project.");
            return;
        }
        visualizationMaterial = new Material(shader);
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField("Flow Map Painter Settings", EditorStyles.boldLabel);
        targetObject = (GameObject)EditorGUILayout.ObjectField("Target Object", targetObject, typeof(GameObject), true);
        brushSize = EditorGUILayout.Slider("Brush Size", brushSize, 1f, 1000f);
        brushStrength = EditorGUILayout.Slider("Brush Strength", brushStrength, 0f, 1f);
        flowMap = EditorGUILayout.ObjectField("Flow Map", flowMap, typeof(Texture2D), false) as Texture2D;

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Visualization Settings", EditorStyles.boldLabel);
        showVisualization = EditorGUILayout.Toggle("Show Visualization", showVisualization);
        visualizationStrength = EditorGUILayout.Slider("Visualization Strength", visualizationStrength, 0f, 1f);
        visualizationAlpha = EditorGUILayout.Slider("Visualization Alpha", visualizationAlpha, 0f, 1f);

        EditorGUILayout.Space();
        if (GUILayout.Button("Create/Reset Flow Map"))
        {
            if (flowMap == null)
                CreateFlowMap();
        }

        if (GUILayout.Button("Export Flow Map"))
        {
            ExportFlowMap();
        }

        EditorGUILayout.HelpBox("Hold left mouse button and drag in the scene view to paint flow direction.", MessageType.Info);
    }

    private void OnSceneGUI(SceneView sceneView)
    {
        if (targetObject == null || flowMap == null) return;

        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

        Event e = Event.current;
        if (e.type == EventType.MouseDown && e.button == 0)
        {
            isPainting = true;
            lastMousePosition = e.mousePosition; // Initialize last mouse position when starting
            e.Use();
        }
        else if (e.type == EventType.MouseUp && e.button == 0)
        {
            isPainting = false;
            e.Use();
        }
        else if (e.type == EventType.MouseDrag && isPainting)
        {
            Vector2 currentMousePosition = e.mousePosition;

            // Calculate flow direction based on the stroke movement (from last to current mouse position)
            Vector2 direction = (currentMousePosition - lastMousePosition).normalized;

            Ray ray = HandleUtility.GUIPointToWorldRay(currentMousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.transform.gameObject == targetObject)
            {
                // Paint using the calculated direction based on mouse drag
                PaintFlowDirection(hit.textureCoord, direction);
            }

            lastMousePosition = currentMousePosition; // Update last mouse position
            e.Use();
            sceneView.Repaint();
        }

        if (showVisualization)
        {
            VisualizeFlowMap(sceneView);
        }
    }

    private void CreateFlowMap()
    {
        if (targetObject == null) return;

        flowMap = new Texture2D(1024, 1024, TextureFormat.RGBAFloat, false);
        Color defaultColor = new Color(0.5f, 0.5f, 1f, 1f); // Default flow direction (0, 0)
        Color[] pixels = new Color[flowMap.width * flowMap.height];
        for (int i = 0; i < pixels.Length; i++)
        {
            pixels[i] = defaultColor;
        }
        flowMap.SetPixels(pixels);
        flowMap.Apply();
    }

    private void PaintFlowDirection(Vector2 uv, Vector2 direction)
    {
        int x = Mathf.FloorToInt(uv.x * flowMap.width);
        int y = Mathf.FloorToInt(uv.y * flowMap.height);

        for (int brushY = -Mathf.FloorToInt(brushSize); brushY <= Mathf.CeilToInt(brushSize); brushY++)
        {
            for (int brushX = -Mathf.FloorToInt(brushSize); brushX <= Mathf.CeilToInt(brushSize); brushX++)
            {
                int sampleX = x + brushX;
                int sampleY = y + brushY;

                if (sampleX >= 0 && sampleX < flowMap.width && sampleY >= 0 && sampleY < flowMap.height)
                {
                    // Calculate distance from the center of the brush stroke
                    float distanceFromCenter = Vector2.Distance(Vector2.zero, new Vector2(brushX, brushY));

                    // If within brush radius
                    if (distanceFromCenter <= brushSize)
                    {
                        // Feathering fall-off calculation (0 at the edge, 1 at the center)
                        float fallOff = Mathf.Clamp01(1f - (distanceFromCenter / brushSize));

                        // Blend fallOff with the brush strength to soften the stroke
                        float effectiveStrength = brushStrength * fallOff;

                        Color existingColor = flowMap.GetPixel(sampleX, sampleY);
                        Vector2 existingDirection = new Vector2(existingColor.r * 2 - 1, existingColor.g * 2 - 1);

                        // Lerp based on the feathered strength
                        Vector2 newDirection = Vector2.Lerp(existingDirection, direction, effectiveStrength);

                        Color newColor = new Color((newDirection.x + 1) * 0.5f, (newDirection.y + 1) * 0.5f, 1f, 1f);
                        flowMap.SetPixel(sampleX, sampleY, newColor);
                    }
                }
            }
        }

        flowMap.Apply();
    }

    private void ExportFlowMap()
    {
        if (flowMap == null) return;

        string path = EditorUtility.SaveFilePanelInProject("Save Flow Map", "FlowMap", "png", "Save flow map texture");
        if (string.IsNullOrEmpty(path)) return;

        byte[] pngData = flowMap.EncodeToPNG();
        System.IO.File.WriteAllBytes(path, pngData);
        AssetDatabase.Refresh();

        TextureImporter importer = AssetImporter.GetAtPath(path) as TextureImporter;
        if (importer != null)
        {
            importer.textureType = TextureImporterType.Default;
            importer.sRGBTexture = false;
            importer.mipmapEnabled = false;
            importer.filterMode = FilterMode.Bilinear;
            importer.wrapMode = TextureWrapMode.Clamp;
            importer.SaveAndReimport();
        }

        Debug.Log("Flow map exported to: " + path);
    }

    private void VisualizeFlowMap(SceneView sceneView)
    {
        if (flowMap == null || targetObject == null || visualizationMaterial == null) return;

        Renderer renderer = targetObject.GetComponent<Renderer>();
        if (renderer == null) return;

        visualizationMaterial.SetTexture("_FlowMap", flowMap);
        visualizationMaterial.SetFloat("_FlowStrength", visualizationStrength);
        visualizationMaterial.SetFloat("_Alpha", visualizationAlpha);

        Graphics.DrawMesh(
            renderer.GetComponent<MeshFilter>().sharedMesh,
            targetObject.transform.localToWorldMatrix,
            visualizationMaterial,
            0,
            sceneView.camera,
            0
        );
    }
}