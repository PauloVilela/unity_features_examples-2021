Shader "Custom/URPFlowMapVisualization"
{
    Properties
    {
        _FlowMap ("Flow Map", 2D) = "white" {}
        _FlowStrength ("Flow Strength", Range(0, 1)) = 0.5
        _Alpha ("Alpha", Range(0, 1)) = 0.7
    }

    SubShader
    {
        Tags {"RenderType" = "Transparent" "RenderPipeline" = "UniversalPipeline" "Queue" = "Transparent"}

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct Attributes
            {
                float4 positionOS : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct Varyings
            {
                float4 positionHCS : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            TEXTURE2D(_FlowMap);
            SAMPLER(sampler_FlowMap);

            CBUFFER_START(UnityPerMaterial)
                float4 _FlowMap_ST;
                float _FlowStrength;
                float _Alpha;
            CBUFFER_END

            Varyings vert(Attributes IN)
            {
                Varyings OUT;
                OUT.positionHCS = TransformObjectToHClip(IN.positionOS.xyz);
                OUT.uv = TRANSFORM_TEX(IN.uv, _FlowMap);
                return OUT;
            }

            half4 frag(Varyings IN) : SV_Target
            {
                float4 flowSample = SAMPLE_TEXTURE2D(_FlowMap, sampler_FlowMap, IN.uv);
                float2 flowDir = (flowSample.rg * 2 - 1) * _FlowStrength;
                
                half3 color = half3(flowDir.x + 0.5, flowDir.y + 0.5, 0.5);
                return half4(color, _Alpha);
            }
            ENDHLSL
        }
    }
}