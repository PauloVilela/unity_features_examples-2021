using UnityEngine;

public class  CB_VertexColourPainter : MonoBehaviour
{
    public Color paintColor = Color.red;
    public float brushSize = 0.1f;
    public float brushStrength = 0.5f;

    private Camera cam;

    void Start()
    {
        cam = Camera.main;
    }

    void Update()
    {
        if (Input.GetMouseButton(0)) // Left mouse button for painting
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                MeshCollider meshCollider = hit.collider as MeshCollider;

                if (meshCollider != null)
                {
                    Mesh mesh = meshCollider.sharedMesh;
                    Vector3[] vertices = mesh.vertices;
                    Color[] colors = mesh.colors;

                    if (colors.Length == 0)
                    {
                        colors = new Color[vertices.Length];
                        for (int i = 0; i < colors.Length; i++)
                            colors[i] = Color.white;
                    }

                    Transform hitTransform = hit.collider.transform;
                    Vector3 hitPoint = hitTransform.InverseTransformPoint(hit.point);

                    for (int i = 0; i < vertices.Length; i++)
                    {
                        float distance = Vector3.Distance(vertices[i], hitPoint);
                        if (distance < brushSize)
                        {
                            colors[i] = Color.Lerp(colors[i], paintColor, brushStrength * (1 - distance / brushSize));
                        }
                    }

                    mesh.colors = colors;
                }
            }
        }
    }
}
